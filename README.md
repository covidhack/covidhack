# COVID-19 Virtual Hackathon

AWS, Fairwinds, Gitlab, and Datadog have teamed up to provide an end-to-end infrastructure solution to enable an application that addresses the pandemic.

This will start with a pitch competition. Using this GitLab project we will accept proposals for projects already underway, or new ideas for projects. Any proposed solution needs to help address at least one of the following:

- Reduce the Spread of the Virus
- Find a Cure for the Virus
- Educate the Public on Covid-19
- Rebuild the Economy

Representatives from each of the backing companies will judge and vote on a solution. The winning project will be announced April 15th. Application development will start immediately after.

* **AWS** will be offering 6 months of application hosting support*
* **GitLab** will be where the project lives
* **Datadog** will be offering free monitoring for 6 months*
* **Fairwinds** will be offering free Kubernetes managed services for 6 months 

We believe with this combination of partners we can provide full infrastructure support to get an idea off the ground, scaling, and moving. Our goal is to address the situation in a practical way, and give our engineering teams and the community a way to be involved.

## Winner

Congratulations to Covid Watch, the winning idea for the pitch phase of this hackathon. We will be coordinating a hackathon to contribute to the Covid Watch codebase **4/23-24** there is need for Android kotlin developers and backend developers. No matter your skill level there's a place for you, you could help with writing automated tests, contribute to the code base, adding comments or documentation.

## Tentative Timeline

**4/22** Details about the structure of the hackathon will be sent to anyone that has signified interest via Slack.

**4/23-24** Coding/Hacking time - Kickoff call details here: https://www.eventbrite.com/e/covid-hackorg-hackathon-in-conjunction-with-covid-watchorg-tickets-103169956116

## Judging

Judges from each company bring technical expertise and have backgrounds in things like microbiology, physics, and healthcare (this list is subject to change):

* Brandon Jung - VP of Alliances at GitLab
* Ilan Rabinovitch - VP of Product and Community at Datadog
* Sarah Zelechoski - VP of Engineering at Fairwinds
* EJ Etherinton - CTO at Fairwinds
* Oxana K. Pickeral, Ph.D., MBA | Global Segment Leader - Healthcare & Life Sciences | AWS Partner Network - from Amazon Web Services
* Bob Wise - General Manager at AWS

## Slack Team

More information about Covid Watch including how to join their Slack Workspace can be found at this [link](https://www.covid-watch.org/collaborate). Within the Covid Watch workspace there is a `#hackathon` channel for discussing this virtual hackathon.
